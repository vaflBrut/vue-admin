// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

window.jQuery = require('jquery');

window.$ = window.jQuery;

require('admin-lte');
require('admin-lte/bootstrap/css/bootstrap.css');
require('font-awesome/css/font-awesome.min.css');
require('ionicons/dist/css/ionicons.css');
require('admin-lte/dist/css/skins/_all-skins.min.css');
require('admin-lte/dist/css/AdminLTE.min.css');

require('admin-lte/plugins/jQuery/jquery-2.2.3.min.js');
require('admin-lte/bootstrap/js/bootstrap.min.js');
require('admin-lte/plugins/slimScroll/jquery.slimscroll.min.js');
require('admin-lte/plugins/fastclick/fastclick.js');
require('admin-lte/dist/js/app.min.js');
require('admin-lte/dist/js/demo.js');

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
});
