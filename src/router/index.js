import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/login';
import Dash from '@/components/dash';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dash',
      component: Dash,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
  ],
});
